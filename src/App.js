import React from 'react';
import logo from './logo.svg';
import './App.scss';
import CategoryData from './js/categories';
import ProductsData from './js/products';

var cat ="";
let activeClass = '';
window.addEventListener('scroll', () => {
	
	if (window.scrollY > 20) {
		document.getElementById('header').classList.add('header-scroll');
	} else {
		document.getElementById('header').classList.remove('header-scroll');
	}


	
});

function App() {

  return (	
	  		<div>
				<section className="welcome-principal">
					<header className={"header fixed-top "+activeClass} id="header">
						<div className="container">
							<a href="#"></a>
							<div id="btn-menu" className="container-btn" href="#menu-principal" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="menu-principal">
								<i className="fas fa-bars"></i>
							</div>
							<nav className="menu-principal collapse" id="menu-principal">
								<ul className="ul-menu">
									<li><a href="#">Nosotros</a></li>
									<li><a href="#container-products">Productos</a></li>
									<li><a href="#">Contacto</a></li>
								</ul>
							</nav>
						</div>
					</header>
					<div className="container-text">
						<section className="text-principal">
							<div className="container">
								<div className="row">
									<div className="col-11 col-sm-11 col-md-11 m-auto">
										<article className="article-text">
											<h1 className="section-title">Mi Venta de Garage</h1>
											<h3 className="section-subtitle">lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem</h3>
										</article>
									</div>
								</div>
							</div>
						</section>
						<section className="section-more">
							<div className="container">
								<div className="row">
									<a href="#" className="btn btn-primary">Conoce más <img src={process.env.PUBLIC_URL +"/images/arrow.png"}/></a>
								</div>
							</div>
						</section>
					</div>
					<div className="img-principal"></div>
				</section>
				<section className = "container-categories" id="container-categories">
					<div className="container">
						<div className="row">
							<section className="section-categories col-12">
								<h3 className="section-title">Categorías</h3>
								<section className="container-categories col-12 col-lg-10 col-xl-7">
									{CategoryData.map((categoryDetail, index) => {
										return <article className="category" key={categoryDetail.id}>
													<div className="category-img"><img src={process.env.PUBLIC_URL +"/images/categories/"+categoryDetail.image} alt={categoryDetail.name}/></div>
													<h5 className="category-title">{categoryDetail.name}</h5>
												</article>
									})}
								</section>
							</section>
						</div>
					</div>
				</section>
				<main className="container-products" id="container-products">
					<div className="container">
						<div className="row">
							<section className="section-products col-12">
								<h3 className="section-title">Productos</h3>
								<section className="container-products col-12 col-lg-11 col-xl-10">
								
									{ProductsData.map((productDetail, index) => {

										{CategoryData.map((categoryDetail, index) => {
											if (productDetail.category_id == categoryDetail.id) {
												cat = categoryDetail.name;
											}
										})}
										
										return <article className={"product "+cat} key={productDetail.id}>
													<span className="product-category">{cat}</span>
													<div className="product-img" style={{backgroundImage: `url(${process.env.PUBLIC_URL}/images/products/${productDetail.image})`}}></div>
													<div className="product-info">
														<h5 className="product-name">{productDetail.name}</h5>
														<span className="product-price">Precio: ${productDetail.price}</span>
													</div>
												</article>
									})}
								</section>
							</section>
						</div>
					</div>
				</main>
				<footer className="footer">
					<div className="container">
						<div className="row">
							<div className="footer-info col-12 col-md-5">
								<p>Owak</p>
								<p>Copyright © 2018</p>
							</div>
							<div className="footer-social col-12 col-md-5">
								<a href="" target="_blank">
									<i className="fab fa-instagram"></i>
								</a>
								<a href="" target="_blank">
									<i className="fab fa-facebook-f"></i>
								</a>
						  		<a href="" target="_blank">
									<i className="fab fa-twitter"></i>
								</a>
							</div>
						</div>
					</div>
				</footer>
		  </div>
        );

}

export default App;
